require('dotenv').config()
const { SPOTIFY_REFRESH_TOKEN, CLIENT_ID, CLIENT_SECRET, PLAYLIST_ID } = process.env

const superagent = require('superagent')

async function sync (spotifyToken) {
  const savedTracksData = []
  const { body: lastSyncedData } = await superagent.get(`https://api.spotify.com/v1/playlists/${PLAYLIST_ID}/tracks`)
    .set('Authorization', spotifyToken)
    .query({
      fields: 'items(track.id),total',
      offset: 0
    })

  const lastSynced = lastSyncedData.items[0].track.id
  for (let i = 0; i < 5; i++) {
    const { body: savedTracksPage } = await superagent.get('https://api.spotify.com/v1/me/tracks')
      .set('Authorization', spotifyToken)
      .query({
        limit: 50,
        offset: i * 50
      })
    if (!savedTracksPage.items) return []
    savedTracksData.push(...savedTracksPage.items)
    const savedTracks = savedTracksData.map(t => t.track.id)
    const lastSyncedIndex = savedTracks.indexOf(lastSynced)
    if (lastSyncedIndex > 0) {
      const songs = []
      songs.push(...savedTracks.slice(0, lastSyncedIndex))
      await superagent.post(`https://api.spotify.com/v1/playlists/${PLAYLIST_ID}/tracks`)
        .set('Authorization', spotifyToken)
        .query({
          position: 0,
          uris: songs.map(id => `spotify:track:${id}`).join(',')
        })
      return songs
    }
  }
  return []
}

function main () {
  superagent.post('https://accounts.spotify.com/api/token')
    .set('Authorization', `Basic ${Buffer.from(`${CLIENT_ID}:${CLIENT_SECRET}`).toString('base64')}`)
    .type('form')
    .send({
      grant_type: 'refresh_token',
      refresh_token: SPOTIFY_REFRESH_TOKEN
    })
    .then(response => {
      sync(`Bearer ${response.body.access_token}`)
        .then(songs => {
          console.log(songs.length ? `Synced ${songs.length} songs` : 'No songs to add')
        })
        .catch(error => {
          console.error(error)
        })
    })
    .catch(error => {
      console.error(error)
    })
}

setInterval(main, 1000 * 60 * 30)
main()
